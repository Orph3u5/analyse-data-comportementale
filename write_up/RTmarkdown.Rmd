---
output:
  word_document: default
  html_document: default
---
```{r, echo= FALSE}
load ("environnement1.RData")
```
#Results 

##Temps de réponse (RT souvenir) 

Le temps de réponse a été enregistré a chaque réponse pendant la partie 2 (celle ou les participants devaient se rappeler d'un souvneir en rapport avec l'item présenté) Les analyses ont été faites par rapport au sexe et a la valence des items (qui ne variaient pas entre les parties 1 et 2), ensuite nous avons fait une analyse par rapport á des variables spécifiques de la partie 2 (la spécificité des souvenirs), enfin nous avons fait des analyses par rapport á la partie 1 également (identification/cristallisation), et par rapport a l'intéraction entre specificite des souvenirs et identification/cristallisation). 

Avant toute chose nous avons transformé la data de RT par un log10 pour que la distribution soit normale. 

#####Figure de distribution de RT_souvenir avant transformation log1
```{r, fig.align='center'}
distrib_RTsouvenir.plot
```

#####Figure de distribution de RT souvenir apres transformation log10
```{r, fig.align='center'}
distrib_RTsouvlog10.plot
```
On voit que il y a des outliers tout de meme, mais la distribution est maintenant normale. 

###En fonction du groupe et de la valence

#####Figure de RT souvenir en fonction du groupe
```{r, fig.align='center'}
RTsouvenir_groupe.plot
```

#####Regression lineaire pour voir l'effet du groupe sur RT souvenir.
```{r}
RTsouvenir_groupe.sum
```

Il y a une tendance pour la différence de RT. Sur la figure nous voyons que il y a plus de variance chez les femmes que chez les hommes. 

#####Figure de RT souvenir en fonction de la valence
```{r, fig.align='center'}
RTsouvenir_valence.plot
```

#####Regression lineaire pour voir l'effet de la valence sur RT souvenir.
```{r}
RTsouvenir_valence.sum
```
On dirait que les participatns se rappelaient plus rapidement d'un souvenir si la valence de l'item etait positive. 

#####Figure de RT souvenir en fonction de groupe*valence
```{r, fig.align='center'}
RTsouvenir_groupe_valence.plot
```

#####Regression lineaire pour voir l'effet de groupe*valence sur RT souvenir.
```{r}
RTsouvenir_valence_gr.sum
```
Nous ne voyons que l'effet de la valence. 

###En fonction de la spécificité 
####Data non-binarisée
On rappelle que on appelle la data non-binarisée les réponses 1/2/3/4, et la data binarisée celle pour laquelle 1/2= sémantique et 3/4 = épisodique. 

#####Figure de RT souvenir en fonction de la spécificité des souvenirs
```{r, fig.align='center'}
RTsouvenir_specificite.plot
```

#####Regression lineaire pour voir l'effet de la spécificité des souvenirs sur RT souvenir.
```{r}
RT_souvenir_specificite.sum
```
Aucun effet significatif n'est remarqué.
#####Figure de RT souvenir en fonction de spécificité des souvenirs*groupe
```{r, fig.align='center'}
RTsouvenir_specificite_groupe.plot
```

#####Regression lineaire pour voir l'effet de spécificité des souvenirs*groupe sur RT souvenir.
```{r}
RT_souvenir_specificite_gr.sum
```
Aucun effet significatif n'est observé (meme si sur la figure on semble voir que les femmes ont une reponse plus rapide pour les souvenirs episodiques). 

#####Figure de RT souvenir en fonction de  spécificité des souvenirs*valence
```{r, fig.align='center'}
RTsouvenir_specificite_valence.plot
```

#####Regression lineaire pour voir l'effet de spécificité des souvenirs*valence sur RT souvenir.
```{r}
RT_souvenir_specificite_val.sum
```
Aucun effet significatif n'est observé
#####Figure de RT souvenir en fonction de spécificité des souvenirs.groupe*valence
```{r, fig.align='center'}
RTsouvenir_specificite_groupe_val.plot
```

#####Regression lineaire pour voir l'effet de spécificité des souvenirs.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_specificite_val_gr.sum
```
Baselines: groupe= Femme, valence= negatif 
on observe une tendnace de l'effet de la valence pour les femmes qui n'est pas observable chez les hommes. 
####Data binarisée

Ici nous avons la spécificité des souvenirs séparée en "sémantique" (reponses 1/2) et "épisodique" (réponses 3/4). 

#####Figure de RT souvenir en fonction de la spécificité des souvenirs
```{r, fig.align='center'}
RTsouvenir_specificiteb.plot
```

#####Regression lineaire pour voir l'effet de la spécificité des souvenirs sur RT souvenir.
```{r}
RT_souvenir_specificiteB.sum
```

Comme pour la data non binarisée on observe aucun effet significatif. 
#####Figure de RT souvenir en fonction de spécificité des souvenirs*groupe
```{r, fig.align='center'}
RTsouvenir_specificite_groupeb.plot
```

#####Regression lineaire pour voir l'effet de spécificité des souvenirs*groupe sur RT souvenir.
```{r}
RT_souvenir_specificite_grB.sum
```
Baselines: groupe= Femme, precision souvenir= episodique
contrairement a la data non-binarisée on observe ici une interaction entre specificité des souvenirs et groupe, ainsi que un effet du groupe quand la data est épisodique (qui disparait quand on met baseline = semantique), et un effet de la precision du souvenir pour les Femmes qui disparait quand on met baseline = Homme (d'apres le plot on voit que les femmes ser rapelleraient des souvenirs plus episodiques plus rapidement que des souvenirs semantiques, et plus rapidement que les hommes). 
Fait interessant, on ne remarque ces differences que sur la data binarisée. 
#####Figure de RT souvenir en fonction de  spécificité des souvenirs*valence
```{r, fig.align='center'}
RTsouvenir_specificite_valenceb.plot
```

#####Regression lineaire pour voir l'effet de spécificité des souvenirs*valence sur RT souvenir.
```{r}
RT_souvenir_specificite_valB.sum
```
Baselines: valence= negatif, specificite souvenir= episodique.
On observe un effet de la valence qui ici est significatif. 

#####Figure de RT souvenir en fonction de spécificité des souvenirs.groupe*valence
```{r, fig.align='center'}
RTsouvenir_specificite_groupe_valb.plot
```

#####Regression lineaire pour voir l'effet de spécificité des souvenirs.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_specificite_val_grB.sum
```
Baselines: groupe= Femme, valence= negatif, precision souvenir= episodique
On a un effet de la precision du souvenir sur les Femmes (et non sur les hommes comme vu dans le test specificité*groupe) qui est significatif, un effet de la valence sur les Femmes et sur les Hommes (significatifs), et un effet du groupe pour les items positifs et negatifs mais uniquement pour les souvenirs episodiques. 
enfin, on a une interaction entre la specificité et le groupe pour les items a valence negative (uniquement tendance pour les items positifs).

###En fonction de l'identification 

#####Figure de RT souvenir en fonction de l'identification
```{r, fig.align='center'}
RTsouvenir_id.plot
```

#####Regression lineaire pour voir l'effet de l'identification sur RT souvenir.
```{r}
RT_souvenir_id.sum
```
On voit que il y a un effet significatif de l'identification. Les participatns semblent se rappeler plus rapidement des souvenirs liés a un item quand celui ci leur correspond. 
#####Figure de RT souvenir en fonction de identification*groupe
```{r, fig.align='center'}
RTsouvenir_id_groupe.plot
```

#####Regression lineaire pour voir l'effet de identification*groupe sur RT souvenir.
```{r}
RT_souvenir_id_gr.sum
```
Baseline: identification= me correspond, groupe=Homme. 

On a un effet significatif de l'identification sur le temps de reponse chez les Hommes. Cet effet disparait quand la baseline du groupe= Femme. 
#####Figure de RT souvenir en fonction de  identification*valence
```{r, fig.align='center'}
RTsouvenir_id_valence.plot
```

#####Regression lineaire pour voir l'effet de identification*valence sur RT souvenir.
```{r}
RT_souvenir_id_val.sum
```
Baselines: valence= positif, identification= me correspond
on observe un effet de l'identification quand la valence est positive qui disparait lorsque la valence est negative, et un effet de la valence quand on a identification= me correspond. Ceci impliquerait que les gens ont un temps de reponse souvenir plus rapide lorsque ils s'identifient a un item positif. 
#####Figure de RT souvenir en fonction de identification.groupe*valence
```{r, fig.align='center'}
RTsouvenir_id_groupe_val.plot
```

#####Regression lineaire pour voir l'effet de identification.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_id_gr_val.sum
```
Baselines: identification= me correspond, groupe= Femme, valence= negatif

On a un effet significatif de l'identification chez les Femmes pour les items negatifs, cet effet disparait chez les hommes ou pour les items positifs. 
On a egalement un effet du groupe pour les items negatifs qui correspondent aux participants.
Il y a un effet de la valence chez les deux sexes mais qui devient uen tendance pour identification= Ne me correspond pas
Nous observons egalement une interaction entre identification et groupe pour les items négatifs (pas positifs), une interactino entre valence et identification et une interaction entre valence et groupe. 
Enfin il y a une interaction significative entre identification, valence et groupe. 
Tout ceci n'avait pas ete observe sur la data non binarisée. 

###En fonction de la cristallisation

#####Figure de RT souvenir en fonction de la cristallisation
```{r, fig.align='center'}
RTsouvenir_crist.plot
```

#####Regression lineaire pour voir l'effet de la cristallisation sur RT souvenir.
```{r}
RT_souvenir_crist.sum
```

#####Figure de RT souvenir en fonction de cristallisation*groupe
```{r, fig.align='center'}
RTsouvenir_crist_groupe.plot
```

#####Regression lineaire pour voir l'effet de cristallisation*groupe sur RT souvenir.
```{r}
RT_souvenir_crist_gr.sum
```
Baselines: cristallisation = cristallise, groupe=Femme

On obtient un effet significatif du groupe pour les items cristallisés qui disparait quand ils ne le sont pas, ainsi qu'une tendance pour la cristallisation chez les Femmes qui n'est pas la chez les Hommes. 
#####Figure de RT souvenir en fonction de  cristallisation*valence
```{r, fig.align='center'}
RTsouvenir_crist_valence.plot
```

#####Regression lineaire pour voir l'effet de cristallisation*valence sur RT souvenir.
```{r}
RT_souvenir_crist_val.sum
```
Baselines: cristallisation= cristallise, valence= positif

Nous avons un effet de la valence sur RT quelle que soit la cristallisation, et un effet de la cristallisationq uand la valence est positive, mais pas qunad la valence est négative. 

#####Figure de RT souvenir en fonction de cristallisation.groupe*valence
```{r, fig.align='center'}
RTsouvenir_crist_groupe_val.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_crist_gr_val.sum
```
Effet de la valence plus fort chez les femmes quand items cristallises. tendance a un effet du groupe pour valence positive
et negative, et effet de la cristallisation pour Femmes et items negatifs.
aucune interaction significative n'est notee. 
###En fonction de identification*cristallisation

#####Figure de RT souvenir en fonction de identification*cristallisation
```{r, fig.align='center'}
RTsouvenir_crist_id.plot
```

#####Regression lineaire pour voir l'effet de  identification*cristallisation sur RT souvenir.
```{r}
RT_souvenir_id_crist.sum
```
Baselines: cristallisation= cristallise, identification= me correspond
On a un effet de la cristallisation quand identification= me correspond, ainsi que un effet de l'identification quand cristallisation = cristallise. 
On voit depuis la figure que la direction de cette difference est dans un temps de reponse plus rapide pour les items cristallises auquels les participants s'identifient. 
#####Figure de RT souvenir en fonction de identification.cristallisation*groupe
```{r, fig.align='center'}
RTsouvenir_crist_id_groupe.plot
```

#####Regression lineaire pour voir l'effet de identification.cristallisation*groupe sur RT souvenir.
```{r}
RT_souvenir_id_crist_gr.sum
```
Baselines: groupe = Femme, identification= me correspond, cristallisation= cristallise
on a un efet de la cristallisation chez les femmes mais pas chez les hommes, ainsi que un effet de l'identification (plus fort chez les hommes que chez les femmes). 
Aucune interaction signficative n'est notee. 
#####Figure de RT souvenir en fonction de  identification.cristallisation*valence
```{r, fig.align='center'}
RTsouvenir_crist_id_valence.plot
```

#####Regression lineaire pour voir l'effet de identificationcristallisation*valence sur RT souvenir.
```{r}
RT_souvenir_id_crist_val.sum
```
Baselines: valence= positif, cristallisation = non-cristallise, identification = me correspond
Il y a ici des differences qui apparaissent uniquement pour les items non cristallises. on a un effet de la valence et une tendance a l'interaction valence*identification

#####Figure de RT souvenir en fonction de identification.cristallisation.groupe*valence
```{r, fig.align='center'}
RTsouvenir_crist_id_groupe_val.plot
```

#####Regression lineaire pour voir l'effet de identification.cristallisation.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_id_crist_gr_val.sum
```
Baselines: groupe=Femme, valence=positif, identification= me correspond, cristallisation = cristallise
On voit un effet de l'interaction valence * groupe et de l'interaction identification.valence, egalement un effet significatif de l'interaction identification.valence.groupe


###En fonction de identification*specificite
####Data non-binarisée

#####Figure de RT souvenir en fonction de identification*spécificité des souvenirs
```{r, fig.align='center'}
RTsouvenir_id_spec.plot
```

#####Regression lineaire pour voir l'effet de  identification*spécificité des souvenirs sur RT souvenir.
```{r}
RT_souvenir_id_spec.sum
```
Aucun effet significatif n'est noté

#####Figure de RT souvenir en fonction de identification.spécificité des souvenirs*groupe
```{r, fig.align='center'}
RTsouvenir_id_groupe_spec.plot
```

#####Regression lineaire pour voir l'effet de identification.spécificité des souvenirs*groupe sur RT souvenir.
```{r}
RT_souvenir_id_spec_gr.sum
```
Baselines: groupe= Femme, identification = me correspond
Aucun effet significatif n'est noté
#####Figure de RT souvenir en fonction de  identification.spécificité des souvenirs*valence
```{r, fig.align='center'}
RTsouvenir_id_valence_spec.plot
```

#####Regression lineaire pour voir l'effet de identification.spécificité des souvenirs*valence sur RT souvenir.
```{r}
RT_souvenir_id_spec_val.sum
```
baselines: valence= positif, identification= me correspond
#####Figure de RT souvenir en fonction de identification.spécificité des souvenirs.groupe*valence
```{r, fig.align='center'}
RTsouvenir_id_groupe_val_spec.plot
```

#####Regression lineaire pour voir l'effet de identification.spécificité des souvenirs.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_id_spec_gr_val.sum
```
Baselines: valence= negatif, groupe = Femme, identification = me correspond. 

Aucun effet significatif n'est noté
####Data binarisée

#####Figure de RT souvenir en fonction de identification*spécificité des souvenirs
```{r, fig.align='center'}
RTsouvenir_id_specb.plot
```

#####Regression lineaire pour voir l'effet de  identification*spécificité des souvenirs sur RT souvenir.
```{r}
RT_souvenir_id_specB.sum
```
Baselines: precision_souvenir= episodique, identification= me correspond
On note un effet de l'identification pour les souvenirs episodiques, cet effet n'est pas visible sur les souvenirs sémantiques. 
#####Figure de RT souvenir en fonction de identification.spécificité des souvenirs*groupe
```{r, fig.align='center'}
RTsouvenir_id_groupe_specb.plot
```

#####Regression lineaire pour voir l'effet de identification.spécificité des souvenirs*groupe sur RT souvenir.
```{r}
RT_souvenir_id_specB_gr.sum
```
Baselines: precision souvenir= episodique, groupe= Femme, identification = me correspond. 
On note un effet du groupe quand les souvenirs sont episodiques et correspondent au participant (temps de reaction plus rapide ches les femmes). egalement on note une interaction significative de la precision du souvenir et du groupe. 

#####Figure de RT souvenir en fonction de  identification.spécificité des souvenirs*valence
```{r, fig.align='center'}
RTsouvenir_id_valence_specb.plot
```

#####Regression lineaire pour voir l'effet de identification.spécificité des souvenirs*valence sur RT souvenir.
```{r}
RT_souvenir_id_spec_valB.sum
```

Baselines: precision souvenir= episodique, valence= positif, identification= me correspond
On a un effet de l'identification sur les items positifs mais pas les negatifs, et une tendance de la valence sur les items qui correspondent aux participants, mais pas sur les items qui ne correspondent pas aux participants.

#####Figure de RT souvenir en fonction de identification.spécificité des souvenirs.groupe*valence
```{r, fig.align='center'}
RTsouvenir_id_groupe_val_specb.plot
```

#####Regression lineaire pour voir l'effet de identification.spécificité des souvenirs.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_id_spec_gr_valB.sum
```
Baselines: identification = me correspond, groupe= Femme, valence= positif, precision souvenir= episodique
On note un effet du groupe pour les items de valence positive, auxquels les participans s'identifient et pour lesquels ils ont des souvenirs episodiques. 
Egalement une tendance a l'interaction groupe*identification. 
###En fonction de cristallisation*spécificité
####Data non-binarisée

#####Figure de RT souvenir en fonction de cristallisation*spécificité des souvenirs
```{r, fig.align='center'}
RTsouvenir_crist_spec.plot
```

#####Regression lineaire pour voir l'effet de  cristallisation*spécificité des souvenirs sur RT souvenir.
```{r}
RT_souvenir_crist_spec.sum
```
Baselines: cristallisation = cristallise
Aucun effet significatif n'a ete noté
#####Figure de RT souvenir en fonction de cristallisation.spécificité des souvenirs*groupe
```{r, fig.align='center'}
RTsouvenir_crist_groupe_spec.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.spécificité des souvenirs*groupe sur RT souvenir.
```{r}
RT_souvenir_crist_spec_gr.sum
```
Baselines: groupe = Femme, cristallisation = cristallise

#####Figure de RT souvenir en fonction de  cristallisation.spécificité des souvenirs*valence
```{r, fig.align='center'}
RTsouvenir_crist_valence_spec.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.spécificité des souvenirs*valence sur RT souvenir.
```{r}
RT_souvenir_crist_spec_val.sum
```
Baselines: valence= positif, cristallisation = cristallise
On a un effet de la valence pour les items cristallises (temps de reponse plus rapide pour les items positifs), egalement une interaction cristallisation *valence. 
De plus on a une triple interaction entre episodicite, cristallisation et valence. 

#####Figure de RT souvenir en fonction de cristallisation.spécificité des souvenirs.groupe*valence
```{r, fig.align='center'}
RTsouvenir_crist_groupe_val_spec.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.spécificité des souvenirs.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_crist_spec_gr_val.sum
```
Baselines: groupe= Femme, valence =positif, cristallisation= cristallise
La aussi nous avons uniquement les effets significatifs de l'interaction valence*cristallisation et la triple interaction entre episodicite, cristallisation et valence. 

####Data binarisée

#####Figure de RT souvenir en fonction de cristallisation*spécificité des souvenirs
```{r, fig.align='center'}
RTsouvenir_crist_specb.plot
```

#####Regression lineaire pour voir l'effet de  cristallisation*spécificité des souvenirs sur RT souvenir.
```{r}
RT_souvenir_crist_specB.sum
```
Baselines: precision souvenir= episodique, cristallisation = cristallise
ON a un effet de la cristallisation pour les souvenirs episodiques, un effet de la precision du souvenir pour les items cristallises, et une interaction cristallisation*precision souvenir. Tout est significatif. 
Les participants ont tendance a se rappeler plus rapidement des souvenirs episodiques pour les items cristallises, et plus rapidement des semantiques pour les items non-cristallises. 

#####Figure de RT souvenir en fonction de cristallisation.spécificité des souvenirs*groupe
```{r, fig.align='center'}
RTsouvenir_crist_groupe_specb.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.spécificité des souvenirs*groupe sur RT souvenir.
```{r}
RT_souvenir_crist_spec_grB.sum
```
Baselines: cristallisation = cristallise, groupe= Femme, precision souvenir= episodique
On a un effet significatif de la cristallisation pour les items cristallises chez les femmes (mais pas chez les hommes).
ON a un effet de la precision des souvenirs sur les Femmes et pour les items cristallises (mais pas les non-cristallises)
ON a egalement un effet significatif du groupe pour les items cristallises (et souvenirs episodiques). Les Femmes semblent se rappeler plus rapidement des souvenirs episodiques cristallises que les hommes, et que des souvenirs semantiques. 
Enfin nous avons une interaction significative entre la precision des souvenirs et le groupe. 

#####Figure de RT souvenir en fonction de  cristallisation.spécificité des souvenirs*valence
```{r, fig.align='center'}
RTsouvenir_crist_valence_specb.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.spécificité des souvenirs*valence sur RT souvenir.
```{r}
RT_souvenir_crist_spec_valB.sum
```
Baselines: valence= positif, cristallisation = cristallise, precision souvenir= episodique
NOus avons un effet de la cristallisation significatif pour les items positifs et les souvenirs episodiques, ainsi que un effet significatif de la valence. Il semblerait que les souvenirs episodiques rattachés a des items positifs soient rememores plus rapidement que les negatifs. 

#####Figure de RT souvenir en fonction de cristallisation.spécificité des souvenirs.groupe*valence
```{r, fig.align='center'}
RTsouvenir_crist_groupe_val_specb.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.spécificité des souvenirs.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_crist_spec_gr_valB.sum
```
Baselines: groupe= Femme, valence= positif, cristallisation = cristallise, precision souvenir= episodique. 

Aucun effet d'interaction n'est noté. 

###En fonction de cristallisation*identification. spécificité
####Data non-binarisée
#####Figure de RT souvenir en fonction de cristallisation.identification*spécificité des souvenirs
```{r, fig.align='center'}
RTsouvenir_crist_id_spec.plot
```

#####Regression lineaire pour voir l'effet de  cristallisation.identification*spécificité des souvenirs sur RT souvenir.
```{r}
RT_souvenir_crist_id_spec.sum
```
Baselines: identification = me correspond, cristallisation = cristallise

Aucun effet significatif n'est noté

#####Figure de RT souvenir en fonction de cristallisationidentification..spécificité des souvenirs*groupe
```{r, fig.align='center'}
RTsouvenir_crist_id_groupe_spec.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.identification.spécificité des souvenirs*groupe sur RT souvenir.
```{r}
RT_souvenir_crist_id_spec_gr.sum
```
Baselines: identification = me correspond, cristallisation = cristallise, groupe= Femme

Aucun effet significatif n'est noté

#####Figure de RT souvenir en fonction de  cristallisation.identification.spécificité des souvenirs*valence
```{r, fig.align='center'}
RTsouvenir_crist_id_valence_spec.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.identification.spécificité des souvenirs*valence sur RT souvenir.
```{r}
RT_souvenir_crist_id_spec_val.sum
```
Baselines: identification = me correspond, cristallisation = cristallise, valence= positif

On a un effet significatif de la valence (identification = me correspond et cristallisation = cristallise). 
On a egalement un effet signficiatif de l'interaction episodicite*valence, ainsi que une triple interaction entre cristallisation, episodicite et valence. 

#####Figure de RT souvenir en fonction de cristallisation.identification.spécificité des souvenirs.groupe*valence
```{r, fig.align='center'}
RTsouvenir_crist_id_groupe_val_spec.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.identification.spécificité des souvenirs.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_crist_id_spec_gr_val.sum
```
Baselines: identification = me correspond, cristallisation = cristallise, valence= positif, groupe = Femme
On a un effet d'interaction entre identification et valence, entre cristallisation et valence et entre cristallisation, identification et valence. Egalement (comme vu rpecedemment) un effet de l'interaction cristallisation.valence.specificite. 



####Data binarisée
#####Figure de RT souvenir en fonction de cristallisation.identification*spécificité des souvenirs
```{r, fig.align='center'}
RTsouvenir_crist_id_specb.plot
```

#####Regression lineaire pour voir l'effet de  cristallisation.identification*spécificité des souvenirs sur RT souvenir.
```{r}
RT_souvenir_crist_id_specB.sum
```
Baselines: identification = me correspond, cristallisation = cristallise
On a un effet de la cristallisation sur les items positifs qui correpsondent aux participants, ainsi que un effet de l'identification sur les items positifs cristallises.  
Aucun effet d'interaction n'est noté. 


#####Figure de RT souvenir en fonction de cristallisationidentification..spécificité des souvenirs*groupe
```{r, fig.align='center'}
RTsouvenir_crist_id_groupe_specb.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.identification.spécificité des souvenirs*groupe sur RT souvenir.
```{r}
RT_souvenir_crist_id_spec_grB.sum
```

Baselines: identification = Ne me correspond pas, cristallisation = cristallise, groupe= Femme
On a un effet de la precision du souvenir pour les items qui ne correpsondent pas aux participants (mais pas ceux qui leurs correspondent) ainsi que un effet du groupe. 
Finalement on a un effet de l'interaction cristallisation*specificite des souvenirs


#####Figure de RT souvenir en fonction de  cristallisation.identification.spécificité des souvenirs*valence
```{r, fig.align='center'}
RTsouvenir_crist_id_valence_specb.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.identification.spécificité des souvenirs*valence sur RT souvenir.
```{r}
RT_souvenir_crist_id_spec_valB.sum
```
Baselines: identification = me correspond, cristallisation = cristallise, valence= positif

On a un effet de la valence ainsi que un effet d'une triple interaction cristallisation, precision souvenir et valence. 
#####Figure de RT souvenir en fonction de cristallisation.identification.spécificité des souvenirs.groupe*valence
```{r, fig.align='center'}
RTsouvenir_crist_id_groupe_val_specb.plot
```

#####Regression lineaire pour voir l'effet de cristallisation.identification.spécificité des souvenirs.groupe*valence sur RT souvenir.
```{r}
RT_souvenir_crist_id_spec_gr_valB.sum
```

Baselines: identification = me correspond, cristallisation = cristallise, valence= positif, groupe = Femme

aucun effet significatif d'interaction n'est noté.