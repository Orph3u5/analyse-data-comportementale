---
title: "Analyses_data_comportementales_neuroimagerie_TRANSMEM"
author: "Laurie Compère"
date: "31 mai 2016"
output: html_document
---

```{r,echo=FALSE}
load("data_comportementales_neuroimagerie_16_06_13.RData")
```

# Analyses sur la part 1
## Reponses
```{r,echo=FALSE}
modele_part1_reponses.lmer_sum
```
=> On voit que pour les items positifs, les participants ont tendance à plus se les attribuer en condition soi qu'autrui.
=> Que pour les items positifs sont plus auto-attribué en condition soi que les items négatifs, même effet pour la condition autrui.
=> Mais l'effet de la valence diffère significativement dans la condition soi et dans la condition fréquence, où il est nettement moins présent.
```{r,echo=FALSE}
plot_part1_reponse_par_condition_valence
```
On regarde s'il y a un gain à rajouter nos variables d'intérêt de sexe et de genre:
```{r,echo=FALSE}
anova(modele_part1_reponses.lmer,modele_part1_reponses_sexe.lmer)
anova(modele_part1_reponses_comparaison_BSRI.lmer,modele_part1_reponses_genre_masculin.lmer)
anova(modele_part1_reponses_comparaison_BSRI.lmer,modele_part1_reponses_genre_feminin.lmer)
```
On voit qu'il y a un gain significatif à rajouter la variable sexe et la variable genre féminin mais pas la variable genre masculin. Et qu'il est plus intéressant d'ajouter la variable genre féminin que la variable sexe.
Une fois que l'on a ajouté le genre féminin dans le modèles, il est encore intéressant de rajouter le groupe :
```{r,echo=FALSE}
anova(modele_part1_reponses_genre_feminin.lmer,modele_part1_reponses_genre_feminin_sexe.lmer)
```
On regarde les résultats du modèle intégrant le genre féminin :
```{r,echo=FALSE}
modele_part1_reponses_genre_feminin.lmer_sum
plot_part1_reponse_par_condition_valence_selon_BSRI_feminin
```
On voit qu'en rajoutant le genre féminin, plus le score de BSRI féminin est important, plus les participants, plus les participants vont avoir tendance à s'attribuer en condition soi les adjectifs de valence positive et moins ils vont avoir tendance à s'attribuer des adjectifs négatifs toujours en conditon soi.
Et comme les intéractions conditionautrui:BSRI_feminin et conditionautrui:valencenegatif:BSRI_feminin ne sont pas significatives, je suppose que l'on a les même tendances en condition autrui.

On peut jeter un coup d'oeil aux résultats du modèle comprenant la variable genre féminin et sexe mais cela devient vraiment plus compliqué à interpréter
```{r,echo=FALSE}
modele_part1_reponses_genre_feminin_sexe.lmer_sum
plot_part1_reponse_par_condition_valence_groupe_selon_BSRI_feminin
```
=> Intéraction BSRI_feminin:groupefemmes significative ce qui signifie qu'en condition soi, pour les items positifs, par rapport aux hommes qui plus ils ont un score de BSRI féminin élevé, plus ils s'autoattribuent des items positifs, on a un effet moins marqué chez les femmes.

## RT
```{r,echo=FALSE}
modele_part1_RT.lmer_sum
```
=> Pour les items positifs, les participants répondent plus vite en condition soi qu'en condition autrui et plus vite en condition fréquence qu'en condition soi.
=> En condition soi, les participants répondent légèrement plus vite aux items positifs que négatifs.
```{r,echo=FALSE}
plot_part1_RT_par_condition_valence
```
Il y a un gain significatif à rajouter le sexe, le genre masculin et le genre féminin dans le modèle. Mais les effets les plus forts sont le sexe et le genre masculin :
```{r,echo=FALSE}
anova(modele_part1_RT.lmer,modele_part1_RT_sexe.lmer)
anova(modele_part1_RT_comparaison_BSRI.lmer,modele_part1_RT_genre_masculin.lmer)
anova(modele_part1_RT_comparaison_BSRI.lmer,modele_part1_RT_genre_feminin.lmer)
```
On regarde les résultats du modèle incluant le sexe :
```{r,echo=FALSE}
modele_part1_RT_sexe.lmer_sum
plot_part1_RT_par_condition_valence_groupe
```
=> On voit que les femmes répondent plus rapidement que les hommes en condition soi pour les items positif
=> Et que l'effet de la valence est significativement plus important chez les femmes car tandis que chez les hommes, il n'y a pas de différence significative en condition soi entre le temps de réponse aux items positifs et négatifs, chez les femmes, le temps de réponse en condition soi est plus important pour le items négatifs que positifs.

On regarde les résultats du modèle incluant le genre masculin :
```{r,echo=FALSE}
modele_part1_RT_genre_masculin.lmer_sum
plot_part1_RT_par_condition_valence_BSRI_masculin
```
=> On voit qu'en condition soi et pour les items positifs, plus les hommes ont un score de BSRI masculin important, plus ils mettent de temps à répondre.
=> On a une intéraction valencenegatif:BSRI_masculin significative dans le sens où l'effet précédemment décrit pour les items positifs est bien moindre pour les items négatifs.

On regarde les résultats du modèle incluant le genre féminin :
```{r,echo=FALSE}
modele_part1_RT_genre_feminin.lmer_sum
plot_part1_RT_par_condition_valence_BSRI_feminin
```
=> En condition soi, pour les items positifs, plus le score de BSRI féminin est élevé, moins les participants répondent rapidement et comme les autres intéractions avec la BSRI féminin ne sont pas significative, cet effet est présent à travers toutes les conditions et valences.


# Analyses sur la part 2
## Reponses
```{r,echo=FALSE}
modele_part2_reponses.lmer_sum
```
=> Les souvenirs sont moins épisodiques en condition souvenir pour les items négatifs que pour les items positifs
```{r,echo=FALSE}
plot_part2_reponse_par_valence
```
Au niveau du type de souvenir récupéré en condition souvenir, il n'y a pas de gain significatif à rajouter le sexe, le genre masculin ni féminin dans le modèle : 
```{r,echo=FALSE}
anova(modele_part2_reponses.lmer,modele_part2_reponses_sexe.lmer)
anova(modele_part2_reponses_comparaison_BSRI.lmer,modele_part2_reponses_genre_masculin.lmer)
anova(modele_part2_reponses_comparaison_BSRI.lmer,modele_part2_reponses_genre_feminin.lmer)
```


## RT
```{r,echo=FALSE}
modele_part2_RT.lmer_sum
```
=> Pour les items positifs, les participants mettent moins de temps à répondre dans la condition définition que dans la condition souvenir.
=> Dans la conditon souvenir, les participants mettent plus de temps à trouver un souvenir pour les items négatifs que pour les items positifs.
=> On a une intéraction significative entre condition et valence car l'effet de la valence est inversé dans la condition définition (cad que les participants trouvent plus vite une définition pour les items négatifs que positifs)
```{r,acho=FALSE}
plot_part2_RT_par_condition_valence
```
Il y a un gain significatif à ajouter le sexe dans le modèle ainsi que le genre masculin et le genre féminin :
```{r,echo=FALSE}
anova(modele_part2_RT.lmer,modele_part2_RT_sexe.lmer)
anova(modele_part2_RT_comparaison_BSRI.lmer,modele_part2_RT_genre_masculin.lmer)
anova(modele_part2_RT_comparaison_BSRI.lmer,modele_part2_RT_genre_feminin.lmer)
```
On regarde ce que donne le modèle avec le genre masculin, pour lequel l'effet est le plus fort :
```{r,echo=FALSE}
modele_part2_RT_genre_masculin.lmer_sum
plot_part2_RT_par_condition_valence_selon_BSRI_masculin
```
=> On n'a pas d'effet significatif du score de BSRI masculin pour les items positifs en condition souvenir, mais on a une intéraction valencenegatif:BSRI_masculin, dans le sens où pour les items négatifs, plus les participants ont un score de BSRI masculin élevé, plus ils mettent de temps à se rappeler des sovenirs => Les individus ayant une identité de genre masculin iraient plus difficilement dans l'épisodicité pour les items négatifs car cela leur prendrait plus de temps (-> représentations négatives d'eux-mêmes moins accessibles ?)

On jette un coup d'oeil au modèle prenant en compte le score de genre féminin :
```{r,echo=FALSE}
modele_part2_RT_genre_feminin.lmer_sum
plot_part2_RT_par_condition_valence_selon_BSRI_masculin
```
=> Effet significatif du score de BSRI féminin pour les items positifs en condition souvenirs, plus les participants ont un score de BSRI feminin important plus ils mettent du temps à récupérer des souvenirs et intéraction valencenegatif:BSRI_feminin non significative donc effet de BSRI_feminin pas différent en condition souvenir pour les items négatifs.

On jette au coup d'oeil au modèle prenant en compte le sexe :
```{r,echo=FALSE}
modele_part2_RT_sexe.lmer_sum
plot_part2_RT_par_condition_valence_sexe
```
=> Il n'y a pas de différence pour les items positifs en condition soi entre les hommes et les femmes. Par contre l'intéraction conditiondefinition:groupefemmes est significative dans le sens où l'effet de la condition varie en fonction du sexe, même s'il va dans le même sens (plus long de retrouver un souvenir que de trouver une définition) et cet écart est plus important chez les femmes.

Quand on ajoute le genre masculin dans le modèle, il est toujours intéressant de rajouter le genre féminin :
```{r,echo=FALSE}
anova(modele_part2_RT_genre_masculin.lmer,modele_part2_RT_genre_masculin_feminin.lmer)
```
On jette un coup d'oeil aux résultats du modèle avec les deux genres :
```{r,echo=FALSE}
modele_part2_RT_genre_masculin_feminin.lmer_sum
```
=> Pour les items positifs en condition souvenir : plus les participants ont un score BSRI masculin élevé, moins ils mettent de temps à répondre à score de BSRI feminin maintenu constant, idem pour le score de BSRI féminin même si coefficient de régression moins important.
=> Intéraction valencenegatif:BSRI_masculin significative plus difficile à interpréter mais l'effet du score de BSRI masculin semble varier avec la valence en condition souvenir (dans le sens où cet effet a l'air plus marqué mais à vérifier ?)
=> Idem pour l'intéraction valencenegatif:BSRI_feminin
=> Intéractions BSRI_masculin:BSRI_feminin et valencenegatif:BSRI_masculin:BSRI_feminin difficile à interpréter 
=> J'ai laissé de coté les résultats concernant la définition car moins intéressants pour l'instant

Quand on a déjà le genre masculin et féminin dans le modèle, il reste intéressant de rajouter le sexe :
```{r,echo=FALSE}
anova(modele_part2_RT_genre_masculin_feminin.lmer,modele_part2_RT_genre_masculin_feminin_sexe.lmer)
```
On jette un coup d'oeil au modèle complet :
```{r,echo=FALSE}
modele_part2_RT_genre_masculin_feminin_sexe.lmer_sum
```
=> Pfiou, il ne ressort rien de significatif mais c'est une bonne indication que ça vaudrait le coup de regarder ce que donnent les modèles genre masculin * sexe et genre feminin * sexe : ils se peut que les variables de genre n'aient pas le même effet selon le sexe d'appartenance.
Effectivement, il y a un gain à rajouter le sexe au modèle genre masculin :
```{r,echo=FALSE}
anova(modele_part2_RT_genre_masculin.lmer,modele_part2_RT_genre_masculin_sexe.lmer)
modele_part2_RT_genre_masculin_sexe.lmer_sum
plot_part2_RT_par_condition_valence_sexe_selon_BSRI_masculin
```
Note : il faudrait certainement transformer les données car ce sont des TR et je ne l'ai pas fait.
Mais en gros en condition souvenir, pour les items positifs, on voit que plus les hommes ont un score de BSRI_masculin positif, plus ils répondent rapidement en condition souvenir, tandis que chez les femmes on a l'effet inverse (BSRI_masculin:groupefemmes).
Chez les hommes, il semble y avoir un amoindrissement de cet effet pour les items négatifs (valencenegatif:BSRI_masculin), et intéraction valencenegatif:BSRI_masculin:groupefemmes significative donc chez les femmes, il semble qu'on ait un effet qui aille dans l'autre sens.

Il y a un gain à rajouter le sexe au modèle genre féminin :
```{r,echo=FALSE}
anova(modele_part2_RT_genre_feminin.lmer,modele_part2_RT_genre_feminin_sexe.lmer)
modele_part2_RT_genre_feminin_sexe.lmer_sum
plot_part2_RT_par_condition_valence_sexe_selon_BSRI_feminin
```
Chez les hommes, plus le score de BSRI_feminin est important, plus ils mettent de temps à répondre en condition souvenir pour les items positifs - intéraction valencenegatif:BSRI_feminin non significative donc grosso modo même effet pour les items négatifs.
Chez les femmes par contre cet effet semble être beaucoup moins important pour les items positifs BSRI_feminin:groupefemmes, voir même inverse (visuellement) et comme cette intéraction est non significative valencenegatif:BSRI_feminin:groupefemmes, je dirai que l'on a un résultat similaire pour les items négatifs.



# Analyses Part 1 & 2
## Effet de l'auto-attribution en condition soi sur le type de souvenir récupéré
Si on traite l'auto-attibution en condition soi comme une variable continue
```{r,echo=FALSE}
modele_part1_part2_reponses_souvenir_soi_continu.lmer_sum
```
=> On voit que globalement pour les items positifs, plus on va s'auto-attribuer les items, plus on aura des souvenirs épisodiques mais que cet effet n'est pas significatif pour les items négatifs sans qu'on ait une intéraction significative, donc on retiendra juste l'effet sur les items positifs.
Il n'y a pas de gain significatif à rajouter le sexe ni le genre féminin dans le modèle, mais il y en a un à rajouter le genre masculin :
```{r,echo=FALSE}
anova(modele_part1_part2_reponses_souvenir_soi_continu.lmer,modele_part1_part2_reponses_souvenir_soi_continu_sexe.lmer)
anova(modele_part1_part2_reponses_souvenir_soi_continu_comparaison_BSRI.lmer,modele_part1_part2_reponses_souvenir_soi_continu_genre_masculin.lmer)
anova(modele_part1_part2_reponses_souvenir_soi_continu_comparaison_BSRI.lmer,modele_part1_part2_reponses_souvenir_soi_continu_genre_feminin.lmer)
```
On regarde les résultats du modèle avec le genre masculin :
```{r,echo=FALSE}
modele_part1_part2_reponses_souvenir_soi_continu_genre_masculin.lmer_sum
plot_part1_part2_reponses_souvenir_soi_continu_selon_BSRI_masculin
```
=> Pour les items positifs, plus le score de BSRI masculin des participants est importants, plus les souvenirs récupérés sont spécifiques
=> Intéraction reponse_soi:BSRI_masculin significative donc cet effet varie significativement en fonction de la réponse donnée en condition soi (selon le degré d'auto-attribution de l'item) : en effet, cela semble être valable surtout pour les réponses peu auto-attribuées (1 et 2) mais pas pour les items fortement auto-attribués pour les items positifs
=> Les intéractions avec la valence ne sont pas significatives donc on peut généraliser ces résultats à tous les items

Si on traite l'auto-attibution en condition soi comme une variable catégorielle (comme on l'avait fait la denière fois)
```{r,echo=FALSE}
modele_part1_part2_reponses_souvenir_soi_categoriel.lmer_sum
```

=> On voit que l'on a rien de significatif
```{r,echo=FALSE}
plot_part1_part2_reponses_souvenir_soi_categoriel
```
Dans ce modèle, par contre, il y a un gain significatif à rajouter le sexe mais pas les variables de genre :
```{r,echo=FALSE}
anova(modele_part1_part2_reponses_souvenir_soi_categoriel.lmer,modele_part1_part2_reponses_souvenir_soi_categoriel_sexe.lmer)
anova(modele_part1_part2_reponses_souvenir_soi_categoriel_comparaison_BSRI.lmer,modele_part1_part2_reponses_souvenir_soi_categoriel_genre_masculin.lmer)
anova(modele_part1_part2_reponses_souvenir_soi_categoriel_comparaison_BSRI.lmer,modele_part1_part2_reponses_souvenir_soi_categoriel_genre_feminin.lmer)
```
On regarde ce que donnent les résultats du modèle avec le sexe :
```{recho=FALSE}
modele_part1_part2_reponses_souvenir_soi_categoriel_sexe.lmer_sum
plot_part1_part2_reponses_souvenir_soi_categoriel_sexe
```
=> On n'a rien de significatif.


## Effet de l'auto-attribution en condition autrui sur le type de souvenir récupéré
Si on traite l'auto-attibution en condition autrui comme une variable continue
```{r,echo=FALSE}
modele_part1_part2_reponses_souvenir_autrui_continu.lmer_sum
```
=> On voit que plus autrui nous attribuerait des items positif, plus on retrouverait des souvenirs épisodiques 
=> On a une tendance à la significativité de cet effet pour les items négatifs dans le même sens
=> Mais effet significativement plus fort pour les items négatifs (cf intéraction)
Il n'y a pas de gain significatif à rajouter le sexe, le genre masculin ou le genre féminin dans le modèle :
```{r,echo=FALSE}
anova(modele_part1_part2_reponses_souvenir_autrui_continu.lmer,modele_part1_part2_reponses_souvenir_autrui_continu_sexe.lmer)
anova(modele_part1_part2_reponses_souvenir_autrui_continu_comparaison_BSRI.lmer,modele_part1_part2_reponses_souvenir_autrui_continu_genre_masculin.lmer)
```


Si on traite l'auto-attibution en condition autrui comme une variable catégorielle (comme on l'avait fait la denière fois)
```{r,echo=FALSE}
modele_part1_part2_reponses_souvenir_autrui_categoriel.lmer_sum
```
=> On a une tendance à la significativité de l'interaction reponse_autrui4:valencenegatif cad que l'on a un effet différent de la valence pour les items fortement auto-attribués en condition autrui
```{r,echo=FALSE}
plot_part1_part2_reponses_souvenir_autrui_categoriel
```
Ici non plus, il n'y a pas de gain significatif à rajouter, le sexe, le genre masculin et le genre féminin dans le modèle :
```{r,echo=FALSE}
anova(modele_part1_part2_reponses_souvenir_autrui_categoriel.lmer,modele_part1_part2_reponses_souvenir_autrui_categoriel_sexe.lmer)
anova(modele_part1_part2_reponses_souvenir_autrui_categoriel_comparaison_BSRI.lmer,modele_part1_part2_reponses_souvenir_autrui_categoriel_genre_masculin.lmer)
anova(modele_part1_part2_reponses_souvenir_autrui_categoriel_comparaison_BSRI.lmer,modele_part1_part2_reponses_souvenir_autrui_categoriel_genre_feminin.lmer)
```


## Hypothese cristallisation soi
```{r,echo=FALSE}
 modele_part1_part2_hypothese_cristallisation_soi.lmer_sum
```
=> On a un effet de la cristallisation pour les items positifs, avec plus de souvenirs épisodiques en condition cristallisee que non cristallisee
=> Un effet de la valence en condition cristallisee, avec moins de souvenirs épisodiques pour les items négatifs que pour les items positifs
=> Intéraction cristallisation:valence non significative donc l'effet de la valence ne diffère pas selon que l'on considère les items donnant lieu à une réponse cristallisée vs non cristallisée
```{r,echo=FALSE}
plot_part1_part2_hypothese_cristallisation_soi
```
Il n'y a pas de gain à rajouter les variables sexe, genre masculin ou genre féminin dans ce modèle :
```{r,echo=FALSE}
anova(modele_part1_part2_hypothese_cristallisation_soi.lmer,modele_part1_part2_hypothese_cristallisation_soi_sexe.lmer)
anova(modele_part1_part2_hypothese_cristallisation_soi_comparaison_BSRI.lmer,modele_part1_part2_hypothese_cristallisation_soi_genre_masculin.lmer)
anova(modele_part1_part2_hypothese_cristallisation_soi_comparaison_BSRI.lmer,modele_part1_part2_hypothese_cristallisation_soi_genre_feminin.lmer)
```


## Hypothese me correspond/ne me correspond pas soi
```{r,echo=FALSE}
modele_part1_part2_hypothese_auto_attribution_soi.lmer_sum
```
=> Pas d'effet significatif de l'auto-attribution ni de la valence
```{r,echo=FALSE}
plot_part1_part2_hypothese_auto_attribution_soi
```
Il n'y a pas d'intérêt à rajouter les variables sexe, genre masculin, genre féminin dans le modèle :
```{r,echo=FALSE}
anova(modele_part1_part2_hypothese_auto_attribution_soi.lmer,modele_part1_part2_hypothese_auto_attribution_soi_sexe.lmer)
anova(modele_part1_part2_hypothese_auto_attribution_soi_comparaison_BSRI.lmer,modele_part1_part2_hypothese_auto_attribution_soi_genre_masculin.lmer)
anova(modele_part1_part2_hypothese_auto_attribution_soi_comparaison_BSRI.lmer,modele_part1_part2_hypothese_auto_attribution_soi_genre_feminin.lmer)
```

# Attention 
Je suis moins sure de moi au niveau de l'interprétation des régressions logistiques qui suivent mais les résultats sont assez concordants avec les régressions linéaires présentées au dessus, ce qui est rassurant.
## Hypothese cristallisation soi avec resume des data en semantique/episodique
```{r,echo=FALSE}
modele_part1_part2_hypothese_cristallisation_soi_epi_sem.glmer_sum
```
=> Il y a moins de souvenirs sémantiques en condition cristallisée que non cristallisée pour les items positifs avec un effet significatif de la valence dans le sens qu'en condition cristallisée, il y a moins de souvenirs épisodiques pour les items négatifs que positifs
=> L'interaction cristallisation/valence n'est pas significative donc l'effet de la valence est le même quelle que soi la cristallisation
```{r,echo=FALSE}
plot_part1_part2_hypothese_cristallisation_soi
```
Il n'y a pas de gain à rajouter le sexe, le genre masculin et le genre féminin dans le modèle :
```{r,echo=FALSE}
anova(modele_part1_part2_hypothese_cristallisation_soi_epi_sem.glmer,modele_part1_part2_hypothese_cristallisation_soi_epi_sem_sexe.glmer)
anova(modele_part1_part2_hypothese_cristallisation_soi_epi_sem_comparaison_BSRI.glmer,modele_part1_part2_hypothese_cristallisation_soi_epi_sem_genre_masculin.glmer)
anova(modele_part1_part2_hypothese_cristallisation_soi_epi_sem_comparaison_BSRI.glmer,modele_part1_part2_hypothese_cristallisation_soi_epi_sem_genre_feminin.glmer)
```

## Hypothese me correspond/ne me correspond pas soi avec resume des data en semantique/episodique
```{r,echo=FALSE}
modele_part1_part2_hypothese_auto_attribution_soi_epi_sem.glmer_sum
```
=> Il n'y a pas d'effet significatif de l'auto-attribution ni de la valence
```{r,echo=FALSE}
plot_part1_part2_hypothese_auto_attribution_soi
```
Il n'y a pas de gain significatif à rajouter le sexe, le genre masculin ou le genre féminin dans le modèle :
```{r,echo=FALSE}
anova(modele_part1_part2_hypothese_auto_attribution_soi_epi_sem.glmer,modele_part1_part2_hypothese_auto_attribution_soi_epi_sem_sexe.glmer)
anova(modele_part1_part2_hypothese_auto_attribution_soi_epi_sem_comparaison_BSRI.glmer,modele_part1_part2_hypothese_auto_attribution_soi_epi_sem_genre_masculin.glmer)
anova(modele_part1_part2_hypothese_auto_attribution_soi_epi_sem_comparaison_BSRI.glmer,modele_part1_part2_hypothese_auto_attribution_soi_epi_sem_genre_feminin.glmer)
```

